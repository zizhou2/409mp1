/* Your JS here. */
console.log('Hello World!')

let index = 0;
showSlides();

document.querySelector('.prev').addEventListener('click', () => {
    --index
    showSlides();
});

document.querySelector('.next').addEventListener('click', () => {
    ++index
    showSlides();
});

function showSlides() {
    const slides = document.querySelectorAll('.slide');
    index = index % 3;
    if (index < 0) {
        index = 2;
    }
    slides.forEach(slide => {
        slide.style.display = "none"
    });
    slides[index].style.display = "block";
}


var modal = document.getElementById("Modal");

var omb = document.getElementById("OpenModal");

var cb = document.getElementById("CloseModal");

omb.onclick = function () {
    modal.style.display = "block";
}

cb.onclick = function () {
    modal.style.display = "none";
}

let nav = document.querySelector('nav');
let h1 = document.querySelector('h1');

window.addEventListener('scroll', function () {
    let scrollPercent = (window.scrollY * 5) / (document.documentElement.scrollHeight - window.innerHeight);
    let navPadding = 50 - scrollPercent * 15;
    let fontSize = 50 - scrollPercent * 15;
    nav.style.padding = navPadding + 'px';
    h1.style.fontSize = fontSize + 'px';
});

const navLinks = document.querySelectorAll(".nav-link");
const sections = document.querySelectorAll("section");

window.addEventListener("scroll", () => {
    let current = "";

    sections.forEach(section => {
        if (window.scrollY > section.offsetTop - section.clientHeight) {
            current = section.getAttribute("id");
        }
    })

    navLinks.forEach(a => {
        a.classList.remove("active");
        if (a.getAttribute("href").substring(1) === current) {
            a.classList.add("active");
        }
    })
});